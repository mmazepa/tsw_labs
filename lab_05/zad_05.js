/*jshint globalstrict: true, devel: true, esversion: 6 */
'use strict';

var ciagFibonacciego;
var ilosc_liczb = 15;

console.log(ilosc_liczb);

// funkcja sprawdzająca poprawność zdefiniowanej funkcji
function checkIt(wersja, fun)
{
    console.time('time');
    ciagFibonacciego = "";
    for (var i = 0; i <= ilosc_liczb; i++)
    {
        ciagFibonacciego = ciagFibonacciego + fun(i) + " ";
    }
    console.log("WERSJA " + wersja + ": CIĄG FIBONACCIEGO");
    console.log("[OD F0 DO F" + ilosc_liczb + " WŁĄCZNIE]:");
    console.log("   " + ciagFibonacciego);
    console.timeEnd('time');
}

// funkcja porównująca działanie obydwu funkcji
function compareBoth(fun1, fun2)
{
    var compareTable = [];
    for (var i = 0; i<= ilosc_liczb; i++)
    {
        if (fun1(i) === fun2(i)) compareTable.push(true);
        else compareTable.push(false);
    }

    console.log("[KOMPARATOR]:");
    if (compareTable.every(function(currentValue)
    {
        if (currentValue) return true;
        else return false;
    }))
    {
        console.log("   Obydwie funkcje dają identyczne wyniki.");
    }
    else
    {
        console.log("   Wyniki nie są identyczne.");
    }
}

// wersja 1: funkcja definiująca ciąg fibonacciego
const fib = (arg)  => {
    if (arg <= 0) {
        return 0;
    }
    if (arg === 1) {
        return 1;
    }
    return fib(arg - 1) + fib(arg - 2);
};

// wesja 1: sprawdzenie poprawności zdefiniowanej funkcji
checkIt(1, fib);
console.log("");

// funkcja "memo"
const memo = (cache, fun) => {
    // var liczba = fun(cache[cache.lenght - 1]);
    // console.log("[LICZBA]: " + liczba);
    // cache.push(liczba);
    // console.log("[CACHE]: " + cache);
    // return cache[cache.lenght - 1];

    if (!cache) cache = [0, 1];
    var liczba = cache[cache.lenght - 1];

    return fun;

    // if (liczba < cache.lenght)
    // {
    //     return cache[liczba];
    // }
    // else
    // {
    //     // cache[liczba] = fun(fib, liczba);
    //     return cache[liczba];
    // }
    // console.log(cache + " | " + fun(fib, 7));
};

// wersja 2: funkcja definiująca ciąg fibonacciego
const fibonacci = memo([0, 1], (recur, n) => {
    return recur(n - 1) + recur(n - 2);
});

// wersja 2: sprawdzenie poprawności zdefiniowanej funkcji
// checkIt(2, fibonacci);
// console.log(fibonacci(10));
console.log(fibonacci);

// sprawdzanie tożsamości obydwu wersji
// compareBoth(fib, fibonacci);
