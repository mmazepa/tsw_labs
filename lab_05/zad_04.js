/*jshint globalstrict: true, devel: true, esversion: 6 */
'use strict';

// ELEMENT TABLICY
const element = {
    cyfra: null,
    sprawdzony: false,
    setNum: function(cyfra)
    {
        this.cyfra = cyfra;
        this.sprawdzony = false;
    }
};

// KODY
var mojKod = [
    new element.setNum(1),
    new element.setNum(2),
    new element.setNum(3),
    new element.setNum(4)
];
var mojKod2 = [
    new element.setNum(1),
    new element.setNum(2),
    new element.setNum(3),
    new element.setNum(1)
];

// RUCHY
var mojRuch = [
    new element.setNum(1),
    new element.setNum(2),
    new element.setNum(3),
    new element.setNum(4)
];
var mojRuch2 = [
    new element.setNum(1),
    new element.setNum(3),
    new element.setNum(7),
    new element.setNum(4)
];
var mojRuch3 = [
    new element.setNum(0),
    new element.setNum(1),
    new element.setNum(4),
    new element.setNum(0)
];
var mojRuch4 = [
    new element.setNum(1),
    new element.setNum(1),
    new element.setNum(1),
    new element.setNum(1)
];
var mojRuch5 = [
    new element.setNum(1),
    new element.setNum(1),
    new element.setNum(2),
    new element.setNum(2)
];

const punkty = {
    czarne: 0,
    biale: 0
};

function wyzerujPunkty()
{
    punkty.czarne = 0;
    punkty.biale = 0;
    mojKod.map((item, index) =>
    {
        item.sprawdzony = false;
    });
}

function wypiszDane(nazwa, tablica)
{
    console.log("[" + nazwa + "]:");
    var str = "";
    tablica.map((item, index) =>
    {
        str = str + item.cyfra + " ";
    });
    console.log("   " + str);
}

const ocena = (kod)  => {
    // zerowanie punktów
    wyzerujPunkty();

    return (ruch) => {
        // implementacja funkcji oceniającej
        console.log("\nMASTERMIND - OCENA");

        // wypisanie kodu i ruchu
        wypiszDane("KOD", kod);

        // wypisanie ruchu
        wypiszDane("RUCH", ruch);

        // sprawdzanie i obsługa ruchu względem kodu
        if (kod.length !== ruch.length)
        {
            console.log("[ERROR]: Długości tablic niezgodne.");
            return false;
        }
        else
        {
            // PUNKTY CZARNE:
            //   1) przyznawanie punktów czarnych
            //   2) oznaczanie sprawdzonych
            kod.map((item, index) =>
            {
                if (item.cyfra === ruch[index].cyfra)
                {
                    punkty.czarne++;
                    kod[index].sprawdzony = true;
                }
            });

            // PUNKTY BIAŁE:
            //   1) sprawdzanie czy już sprawdzone
            //   2) jeśli nie, to przyznawanie białych punktów
            kod.map((item1, index1) =>
            {
                ruch.map((item2, index2) =>
                {
                    if (item1.cyfra === item2.cyfra && !item1.sprawdzony)
                    {
                        punkty.biale++;
                        kod[index1].sprawdzony = true;
                    }
                });
            });

            return punkty;
        }
    };
};

console.log(ocena(mojKod)(mojRuch));
console.log(ocena(mojKod)(mojRuch2));
console.log(ocena(mojKod)(mojRuch3));
console.log(ocena(mojKod)(mojRuch4));
console.log(ocena(mojKod2)(mojRuch5));
