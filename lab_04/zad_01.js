/*jshint globalstrict: true, devel: true, esversion: 6 */
'use strict';

var defFun = function (fun, types) {
  fun.typeConstr = types;
  return fun;
};

var myFun = defFun((a, b) => a + b, ['number', 'number']);

var appFun = function (f, ...elements) {
  if (f.typeConstr === 'undefined') {
    throw({ typerr: "typeConstr is undefined" });
  }

  let args = [];
  for (let i = 1; i < elements.length; i++) {
    if (typeof elements[i] !== f.typeConstr[i-1]) {
      let errorMsg = "Type of arg" + (i-1) + " ('" + typeof elements[i] + "')" +
        " does not match typeConstr[" + (i-1) + "] ('" + f.typeConstr[i-1] + "')";
      throw({ typerr: errorMsg });
    }
    else args.push(elements[i]);
  }
  console.log("Argumenty: " + elements);
  return f.apply(this, elements);
};

try {
  console.log("Wynik: " + appFun(myFun, 12, 15));
} catch (e) {
  console.log(e.typerr);
}
