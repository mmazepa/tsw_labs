/*jshint globalstrict: true, devel: true, esversion: 6 */
'use strict';

var Template = function (body) {
  this.body = body;
  this.podstaw = function (data) {
    return body.replace(/\{([a-zA-Z0-9]+)\}/g, (match, arg) => {
      if (data.hasOwnProperty(arg)) {
        return data[arg];
      }
      else {
        return match;
      }
    });
  };
};

var szablon =
  '<table border="{border}">\n' +
  '  <tr>\n' +
  '    <td>{first}</td>\n' +
  '    <td>{last}</td>\n' +
  '  </tr>\n' +
  '</table>';

var dane = {
  first: "Jan",
  last: "Kowalski",
  pesel: "97042176329"
};

var myTemplate = new Template(szablon);
console.log(myTemplate.podstaw(dane));
