/* jshint browser: true, esversion: 6, globalstrict: true, devel: true, jquery: true */
/* global io: false */
/* globals doT, _ , moment*/
"use strict";
// Inicjalizacja UI

$(function ()
{
	var username,
	selectedChannel,
	activeChannel,
	channels = {},
	systemChannel;

	var initSystemChannel = function initSystemChannel()
    {
		systemChannel = io('http://' + location.host + '/system');

		systemChannel.on('connect', function ()
        {
			console.log('connected to /system hidden channel');
		});

		systemChannel.on('system-message', function (systemMessage)
        {
			var presence, body, channel;
			console.log(systemMessage);
			if (systemMessage.channelPresence)
            {
				presence = systemMessage.channelPresence;
				body = presence.createdBy +
			 				 ' created channel "' + presence.name + '".';
				channel =
                {
					id: presence.id,
					name: presence.name
				};
				addChannelToList(channel);
			}
			if (systemMessage.userPresence)
            {
				// ...
			}
		});
	};

	var createNewChannel = function createNewChannel(name)
    {
        var createdChannels = Array.from(document.getElementsByClassName("channel-name"));
        var isDuplicate = false;
        var isValid = false;

        for (let i = 0; i < createdChannels.length; i++)
        {
            let currentChannel = createdChannels[i].innerHTML.toString().toLowerCase();
            let newName = name.toLowerCase();

            if (currentChannel === newName)
            {
                isDuplicate = true;
                alert("Channel with that name already exists!");
                break;
            }

            if (newName.replace(/[^a-z0-9]/gi, '') !== newName)
            {
                isValid = false;
                alert("This channel name is not valid!");
                break;
            }
            else
            {
                isValid = true;
            }
        }

        if (!isDuplicate && isValid)
        {
            $.ajax({
              url: "/channel/new/",
              type:"POST",
              data: JSON.stringify({name: name}),
              contentType:"application/json; charset=utf-8",
              dataType:"json",
              success: function(data)
              {
                console.log('Channel "%s" has been created with id: "%s"', data.name, data.id);
                return data;
              }
            });
        }
	};

	var connectToChannel = function connectToChannel(channel)
    {
		if (activeChannel)
        {
			$('#' + activeChannel.channelInfo.id + ' .status').text('');
			activeChannel.disconnect();
			activeChannel = null;
		}
		activeChannel = io('http://' + location.host + '/' + channel.id);
		activeChannel.channelInfo = channel;

		activeChannel.on('message', function (message)
        {
			if (message.from === username)
            {
				return;
			}
			console.log(message);
			channels[activeChannel.channelInfo.id].history.push(message);
			$('#messages-list').append(doT.template($('#their-message').text())(message));
			$("#messages-list").animate({ scrollTop: $('#messages-list').prop("scrollHeight")}, 300);
		});

		$('#messages-list').html('');
		if (channels[activeChannel.channelInfo.id].history.length > 0)
        {
			_.forEach(channels[activeChannel.channelInfo.id].history, function (message)
            {
				if (message.from == username)
                {
					$('#messages-list').append(doT.template($('#my-message').text())(message));
				}
				else
                {
					$('#messages-list').append(doT.template($('#their-message').text())(message));
				}
			});
		}
	};

	var addChannelToList = function addChannelToList(channel)
    {
        $('#channel-list').append(doT.template($('#channel-list-item').text())(channel));
        channels[channel.id] = channel;
        channels[channel.id].history = [];
        $('#' + channel.id).click(function ()
        {
            if (selectedChannel)
            {
                $(selectedChannel).removeClass("channel-selected");
            }
            selectedChannel = this;
            $(this).toggleClass("channel-selected");
        });
	};

	var sendMessage = function sendMessage(event)
    {
		var body = $('#send-message-input').val();
		if (body === '')
        {
			return false;
		}
		var message =
        {
			from: username,
			body: body,
			time: moment().calendar()
		};
		activeChannel.emit('message', message);
		channels[activeChannel.channelInfo.id].history.push(message);
		$('#messages-list').append(doT.template($('#my-message').text())(message));
		$("#messages-list").animate({ scrollTop: $('#messages-list').prop("scrollHeight")}, 300);
		$('#send-message-input').val('');
		return false; //same as event.preventDefault();
	};

	$("#join-button").click(function ()
    {
		if (!selectedChannel)
        {
			alert("Oops... You need to select a channel first!");
			return false;
		}
		if (!activeChannel)
        {
			$('#chat-window').css('display', 'block');
		}
		var channelId = $(selectedChannel).attr("id");
		connectToChannel(channels[channelId]);
		$('#' + channelId + ' .status').text('✪');
	});

	$("#create-button").click(function ()
    {
		var channel;
        var channelName = prompt("Enter channel name (alphanumeric only):");
        if (channelName != null && channelName != "")
        {
            channel = createNewChannel(channelName);
        }
        else
        {
            alert("You need to write something!");
        }
	});

	$('#send-message-form').submit(sendMessage);
	$('#send-message-button').click(sendMessage);

	(function registerUsername()
    {
        var nickname = prompt("Hello there! Pick a nickname:");
        if (nickname != null & nickname != "")
        {
            username = nickname;
            $("#username").text(username);
            initSystemChannel();
        }
        else
        {
            alert("You need to write something!");
            location.reload(true);
        }
	})();

	(function updateChannels()
    {
		$.post("/channels", function (data)
        {
			channels = data;
			_.forEach(channels, function (channel)
            {
				addChannelToList(channel);
			});
		});
	})();
});
