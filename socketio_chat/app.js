/* jshint esversion: 6, node: true */
'use strict';
var express = require('express');
var app = express();
var serveStatic = require("serve-static");

var httpServer = require("http").createServer(app);

var socketio = require("socket.io");
var io = socketio.listen(httpServer);
var bodyParser = require('body-parser');
var _ = require('lodash');

app.use(serveStatic("public"));
app.use(serveStatic("node_modules/jquery/dist"));
app.use(serveStatic("node_modules/moment"));
app.use(serveStatic("node_modules/dot"));
app.use(serveStatic("node_modules/lodash"));
app.use(bodyParser.json());

var channels = {},
    systemChannel;

app.post('/channels/', function (req, res)
{
  let toRet = {};
  _.forEach(channels, function (value, key)
  {
    toRet[key] =
    {
      id: value.id,
      name: value.name
    };
  });
  res.setHeader('Content-Type', 'application/json');
  res.json(toRet);
});
app.post('/channel/new/', function (req, res)
{
  let name = req.body.name;
  let id = _.kebabCase(name);
  channels[id] =
  {
    name: name,
    id: id,
    channel: io
      .of('/' + id)
      .on('connection', function (socket)
      {
        console.log('Uruchomiłem kanał "/' + id + '"');
          socket.on('message', function (data)
          {
              console.log('/' + id + ':\n' + JSON.stringify(data, null, 2));
              channels[id].channel.emit('message', data);
          });
      })
  };
  systemChannel.emit('system-message',
  {
    channelPresence:
    {
      created: true,
      name: name,
      id: id
    }
  });
  res.json({name: name, id: id});
});

channels.default =
{
  name: "Default",
  id: "default",
  channel: io
    .of('/default')
    .on('connection', function (socket)
    {
    	console.log('Otrzymano połączenie do kanału "/default"');
        socket.on('message', function (data)
        {
            console.log('/default:\n' + JSON.stringify(data, null, 2));
            channels.default.channel.emit('message', data);
        });
    })
};

channels.bonus =
{
  name: "Bonus",
  id: "bonus",
  channel: io
    .of('/bonus')
    .on('connection', function (socket)
    {
    	console.log('Otrzymano połączenie do kanału "/bonus"');
        socket.on('message', function (data)
        {
            console.log('/bonus:\n' + JSON.stringify(data, null, 2));
            channels.bonus.channel.emit('message', data);
        });
    })
};

systemChannel = io
  .of('/system')
  .on('connection', function (socket)
  {
      console.log('someone has connected to app');
      socket.on('system-message', function (data)
      {
        console.log('/system:\n' + JSON.stringify(data, null, 2));
        systemChannel.emit('system-message', data);
      });
  });

httpServer.listen(3000, function ()
{
    console.log('[Serwer HTTP]: nasłuchiwanie na porcie 3000.');
});
