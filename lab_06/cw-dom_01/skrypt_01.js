//jshint browser: true, esversion:6

$(() => {
    // kliknięcie raz zmienia kolor wiersza na żółty
    // (dodanie klasy "yellow")
    $("table tr").on("click", function(e)
    {
        $("table tr").siblings().removeClass("yellow");
        $(this).addClass("yellow");
    });

    // odkliknięcie poza tabelką przywraca kolor domyślny
    // (usunięcie klasy "yellow")
    $(document).on("click", function(e)
    {
        if (!($(e.target).closest("table tr").length)) {
            $("table tr").siblings().removeClass("yellow");
        }
    });

    // dwuklik zamienia element na pole tekstowe, które
    // po zakończeniu edycji wraca do stanu pierwotnego
    // a zmiany w tekście zostają
    $("table tr td").on("dblclick", function()
    {
        var myElem = $(this);
        var myInput = $("<input>",
        {
            value: myElem.text(),
            type: "text",
            class: "textField",
            blur: function()
            {
               myElem.text(this.value);
            },
            keyup: function(e)
            {
               if (e.which === 13) myInput.blur();
            }
        }).appendTo(myElem.empty()).focus();
    });

    // przemieszczanie żółtego wiersza za pomocą strzałek
    $(document).keyup(function(e)
    {
        var myKey = e.key;
        var myElem = $(document).find("table tr.yellow");

        var currentRow = myElem;
        var previousRow = myElem.prev();
        var nextRow = myElem.next();

        if (myKey === "ArrowUp")
        {
            if (myElem.prev())
            {
                currentRow.after(previousRow);
            }
        }

        if (myKey === "ArrowDown")
        {
            if (myElem.next())
            {
                currentRow.before(nextRow);
            }
        }
    });
});
