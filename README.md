# Technologie Sieci WEB #

Repozytorium na zadania z poszczególnych laboratoriów.

## Student ##

| Imię i nazwisko | Numer indeksu | Cel repozytorium |
|-----------------|---------------|------------------|
| Mariusz Mazepa  | 235371        | Laboratorium TSW |

## Mastermind ##

| L.P. | Istotne    |
|------|------------|
| 1    | Ajax       |
| 2    | POST, JSON |
| 3    | jQuery     |

## Projekt ##

**Temat:** micro-eBay (serwis aukcyjny)

Szczegóły na stronie prowadzącego zajęcia:
https://inf.ug.edu.pl/~wpawlowski/lab/TSW/
(sekcja **Projekt Indywidualny**).

## Informacje końcowe ##

W przypadku, kiedy ocena końcowa będzie się wahać,
istotna może się okazać implementacja Mastermind.
