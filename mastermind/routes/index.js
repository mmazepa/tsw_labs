//jshint node: true, esversion: 6

var _ = require('underscore'),
    getMarks = require('../logic').getMarks;

exports.index = function (req, res)
{
    req.session.puzzle = req.session.puzzle || req.app.get('puzzle');
    res.render('index',
    {
        title: 'Mastermind',
        level:
        {
            practice:
            {
                name: "Practice",
                tries: "unlimited",
                triesNum: 0
            },
            easy:
            {
                name: "Easy",
                tries: "50",
                triesNum: 50
            },
            medium:
            {
                name: "Medium",
                tries: "30",
                triesNum: 30
            },
            hard:
            {
                name: "Hard",
                tries: "20",
                triesNum: 20
            },
            expert:
            {
                name: "Expert",
                tries: "10",
                triesNum: 10
            }
        }
    });
};

exports.play = function (req, res)
{
    var returnObj = {};
    var newGame = function ()
    {
        var i, data = [],
            puzzle = req.session.puzzle;
        for (i = 0; i < puzzle.size; i += 1)
        {
            data.push(Math.floor(Math.random() * puzzle.dim));
        }
        req.session.puzzle.data = data;
        returnObj.puzzle = puzzle;
    };
    req.session.puzzle = req.session.puzzle || req.app.get('puzzle');

    // req.params[2] === wartość size
    // req.params[4] === wartość dim
    // req.params[6] === wartość max

    if (req.params[2])
    {
        req.session.puzzle.size = req.params[2];
    }
    if (req.params[4])
    {
        req.session.puzzle.dim = req.params[4];
    }
    if (req.params[6])
    {
        req.session.puzzle.max = req.params[6];
    }

    newGame();
    res.render('_play',
    {
        title: 'Mastermind',
        size: req.session.puzzle.size,
        dim: req.session.puzzle.dim,
        max: req.session.puzzle.max
    },
    function (err, view)
    {
      returnObj.view = view;
    });
    res.json(returnObj);
};

exports.mark = function (req, res)
{
    var markAnswer = function ()
    {
        var move = req.params[0].split('/');
        move = move.map(function(x)
        {
            return parseInt(x);
        });
        move = move.slice(0, move.length - 1);

        var code = req.session.puzzle.data;
        code = code.map(function(x)
        {
            return parseInt(x);
        });

        console.log(code);
        console.log(move);

        if (code.length == move.length && code.every((v,i) => v === move[i]))
        {
            console.log("You won!");
        }
        else
        {
            console.log("Try again!");
        }

        return getMarks(code, move);
    };
    res.json(markAnswer());
};
