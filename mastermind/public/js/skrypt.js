/*jshint jquery: true, devel: true, esversion: 6, browser: true */

$(function () {
    $('#playButton').click((event) => {
      var size = $('#sizeValue').val(),
          dim  = $('#dimValue').val(),
          max  = $('#levelValue').val();

      if (!size) {
        size = 5;
      }
      if (!dim) {
        dim = 9;
      }

      if (parseInt(size) <= 0 || parseInt(dim) <= 0)
      {
          alert("Size and dimensions must be greater than zero!");
          location.reload();
      }

      var playUrl = "/play/size/" + size + "/dim/" + dim + "/max/" + max + "/";

      var counter = 1;
      $.get(playUrl, (data) => {
        $("#mainContent").html(data.view);
        $("#guessButton").click((event) => {
          var markUrl = "/mark/";
          var answers = [];
          $(".answerInput").val((index, value) => {
            if (!value) {
              value = 0;
            }
            markUrl += value + "/";
            answers.push(value);
          });

          var blackCounter = 0;
          $.get(markUrl, (data) => {
            var smallerMarks = [];
            for (let i = 0; i < data.black; i++) {
              smallerMarks.push("●");
              blackCounter++;
            }

            for (let i = 0; i < data.white; i++) {
              smallerMarks.push("○");
            }

            if (smallerMarks.length === 0)
            {
                smallerMarks.push("no points");
            }

            var previousMarks = document.getElementById("previousMarks");
            var newDiv = document.createElement("div");
            newDiv.setAttribute("class", "markValues");
            var textNodeString = "[" + counter++ + "]: " + answers + " - " + smallerMarks;
            var maximum = parseInt(max)+1;
            var mySize = parseInt(size);

            if (blackCounter === mySize || counter === maximum)
            {
                document.getElementById("guessButton").classList.add("myButtonDisabled");
                document.getElementById("guessButton").disabled = true;

                if (blackCounter === mySize) textNodeString += " - YOU WON!";
                if (counter === maximum) textNodeString += " - YOU LOSE!";

                var mainContent = document.getElementById("mainContent");
                var newButton = document.createElement("input");
                newButton.setAttribute("class", "myButton");
                newButton.setAttribute("type", "submit");
                newButton.setAttribute("value", "Main Menu");
                newButton.setAttribute("onclick", "location.reload();");
                mainContent.append(newButton);
            }

            newValues = document.createTextNode(textNodeString);
            newDiv.append(newValues);
            previousMarks.append(newDiv);
            previousMarks.scrollTop = 100*counter;
          });
          $("#triesLeft").text((index, text) => {
            if (text.trim() === "No limit") {
              return text;
            }
            return text-1;
          });
        });
      });
    });
});
