//jshint node: true, esversion: 6

var mongoose = require("mongoose");
// var mongoosePaginate = require("mongoose-paginate");
// var mongoosePages = require("mongoose-pages");

var auctionSchema = mongoose.Schema(
{
    title: { type: String, required: true },
    author: { type: String, required: true },
    type: { type: String, required: true },
    price: { type: Number, required: true, default: 1.00 },
    createDate: { type: Date, required: false, default: Date.now },
    duration: { type: Number, required: false },
    expireDate: { type: Date, required: false },
    description: { type: String, required: false },
    isAvailable: { type: Boolean, required: true },
    buyer:
    {
        email: { type: String, required: false },
        date: { type: Date, required: false, default: Date.now }
    },
    bids:
    [{
        email: { type: String, required: false },
        date: { type: Date, required: false, default: Date.now },
        bid: { type: Number, required: false }
    }],
    messages:
    [{
        user: { type: String, required: false },
        content: { type: String, required: false },
        time: { type: String, required: false }
    }]
});

auctionSchema.methods.prepare = function ()
{
    type = this.type;
    duration = this.duration;

    if (type === "Kup teraz!")
    {
        this.duration = null;
        this.expireDate = null;
    }
    else if (type === "Licytacja")
    {
        if (!duration) this.duration = 14;
        this.expireDate = new Date(this.createDate.getTime() + 1000 * 3600 * 24 * this.duration);
    }
    else
    {
        // ani kup teraz, ani licytacja...
    }

    if (this.expireDate && this.expireDate < new Date())
    {
        this.isAvailable = false;
    }
    else
    {
        this.isAvailable = true;
    }
};


// auctionSchema.plugin(mongoosePaginate);
// mongoosePages.skip(auctionSchema);
// var docsPerPage = 10;
// var pageNumber = 1;
//
var Auction = mongoose.model("Auction", auctionSchema);
// Auction.findPaginated({}, function (err, result)
// {
//     if (err) throw err;
//     console.log(result);
// }, docsPerPage, pageNumber);

module.exports = Auction;
