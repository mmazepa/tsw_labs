/*jshint node: true, esversion: 6 */

var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var ejs = require("ejs");
var errorHandler = require("errorhandler");
var nodeMailer = require("nodemailer");

var User = require("./models/User");
var Auction = require("./models/Auction");

var routes = require("./routes/index");
var users = require("./routes/users");
var auctions = require("./routes/auctions");

var port = process.env.PORT || 3000;
var env = process.env.NODE_ENV || "development";
var secret = process.env.SECRET || "$uper $ecret";

var flash = require('connect-flash');
var session = require('express-session');

var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// MongoDB/Mongoose
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/mydb");
var db = mongoose.connection;

// Websocket/Socket.IO
io.on("connection", function(socket)
{
    let room;
    let user;
    let roomId;

    socket.on('room', function(roomInfo)
    {
        socket.join(roomInfo.name);
        room = roomInfo.name;
        user = roomInfo.user;
        roomId = roomInfo.id;
        console.log("[POKÓJ]: " + user + " dołączył do: " + room);
    });

    socket.on("disconnect", function()
    {
        console.log("[POKÓJ]: " + user + " opuścił: " + room);
        socket.leave(room);
    });
    socket.on('chat message', function(msg)
    {
        let currentTime = new Date(msg.time);
        let day = currentTime.getDate();
        let month = currentTime.getMonth()+1;
        let year = currentTime.getFullYear();
        let hours = currentTime.getHours();
        let minutes = currentTime.getMinutes();
        let seconds = currentTime.getSeconds();

        if (day < 10) day = '0' + day;
        if (month < 10) month = '0' + month;
        if (hours < 10) hours = '0' + hours;
        if (minutes < 10) minutes = '0' + minutes;
        if (seconds < 10) seconds = '0' + seconds;

        currentTime = day + '-' + month + '-' + year + " " + hours + ":" + minutes + ":" + seconds;

        console.log('[' + msg.room + " | " + currentTime + '] ' + msg.author + ": " + msg.content);
        io.to(room).emit('chat message', msg);

        message =
        {
            user: msg.author,
            content: msg.content,
            time: msg.time
        };
        Auction.findByIdAndUpdate(roomId,
        {
            $push:
            {
              messages: message
            }
        },
        { new: true },
        function(err, auction)
        {
          if (err)
          {
            console.log(err);
          }
        });
    });
});

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.once('open', function()
{
    console.log("[Mongo]:    MongoDB uruchomiony!");

    // MongoDB Default Auctions

    // --- CLEAR DB ----------

    Auction.deleteMany({}, function(err) {});
    console.log("[Mongo]:    Baza wyczyszczona!");

    // --- AUCTIONS ----------

    var auction1 = new Auction(
    {
        title: "Allview P8 Energy Mini",
        author: "maksymilianmariusz@gmail.com",
        type: "Kup teraz!",
        price: 25.14,
        createDate: "2018-05-20T23:21",
        description: "Smartfon Dual SIM, akumulator 4000 mAh," +
                     " Tryb Inteligentnego Oszczędzania Energii," +
                     " funkcja power bank."
    });

    var auction2 = new Auction(
    {
        title: "Lenovo Y500",
        author: "maksymilianmariusz@gmail.com",
        type: "Licytacja",
        price: 2.50,
        createDate: "2018-06-01T08:15",
        description: "Laptop firmy Lenovo, kolor podświetlenia czerwony," +
                     " 12 GB RAM."
    });

    var auction3 = new Auction(
    {
        title: "Cuboid 150W",
        author: "mmazepa@sigma.ug.edu.pl",
        type: "Kup teraz!",
        price: 129.99,
        createDate: "2018-04-20T14:34"
    });

    var auction4 = new Auction(
    {
        title: "ART Optical Mouse AM92",
        author: "kefas@wp.pl",
        type: "Licytacja",
        createDate: "2018-05-20T23:21"
    });

    var auction5 = new Auction(
    {
        title: "SMOK TFV8 Baby Beast",
        author: "mmazepa@sigma.ug.edu.pl",
        type: "Kup teraz!",
        price: 89.50,
        createDate: "2018-05-20T23:21"
    });

    var auction6 = new Auction(
    {
        title: "Marshall Major II",
        author: "kefas@wp.pl",
        type: "Kup teraz!",
        price: 50.00
    });

    var auction7 = new Auction(
    {
        title: "DR NEON Green Bass Strings",
        author: "grunwald@jagiello.pl",
        type: "Licytacja",
        price: 4.50
    });

    // --- SAVING TO DB ----------

    function saveToDB(element)
    {
        element.prepare();
        element.save(function (err, element)
        {
            if (err) return console.error(err);
            console.log("Aukcja \"" + element.title + "\" zapisana w bazie danych.");
        });
    }

    console.log("");
    saveToDB(auction1);
    saveToDB(auction2);
    saveToDB(auction3);
    saveToDB(auction4);
    saveToDB(auction5);
    saveToDB(auction6);
    saveToDB(auction7);
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ secret: secret }));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
  },
  function(username, password, done)
  {
    User.findOne({ username: username }, function (err, user)
    {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      if (!user.verifyPassword(password)) { return done(null, false); }
      return done(null, user);
    });
  }
));

app.use(flash());

app.use("/", routes);
app.use("/users", users);
app.use("/auctions", auctions);

require('./config/passport')(passport);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

if (env === 'development')
{
    app.use(logger('dev'));
    app.use(errorHandler());
}
else
{
    app.use(logger('short'));
}

app.use(function(err, req, res, next)
{
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

http.listen(port, function()
{
    console.log("       _                    _____         ");
    console.log(" _____|_|___ ___ ___    ___| __  |___ _ _ ");
    console.log("|     | |  _|  _| . |  | -_| __ -| .'| | |");
    console.log("|_|_|_|_|___|_| |___|  |___|_____|__,|_  |");
    console.log("                                     |___|");
    console.log("");
    console.log("[Serwer]:   Nasłuchuję na porcie " + port + ".");
});

module.exports = app;
