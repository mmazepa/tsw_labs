//jshint node: true, esversion: 6

var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/User');
var session;

module.exports = function(passport)
{
  passport.serializeUser(function(user, done)
  {
    done(null, user.id);
  });
  passport.deserializeUser(function(id, done)
  {
    User.findById(id, function(err, user)
    {
      done(err, user);
    });
  });

  passport.use('local-signup', new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
  },
  function(req, email, password, done)
  {
    session = req.session || session;
    process.nextTick(function()
    {
      User.findOne({ 'local.email':  email }, function(err, user)
      {
        if (err)
            return done(err);
        if (user)
        {
          return done(null, false, req.flash('signupMessage', 'That email is already in use.'));
        }
        else
        {
          var newUser = new User();
          newUser.local.email = email;
          newUser.local.password = newUser.generateHash(password);
          newUser.save(function(err)
          {
            if (err)
              throw err;
            console.log("ZAREJESTROWANO: " + newUser.local.email + ", " + newUser.local.password);
            return done(null, newUser);
          });
        }
      });
    });
  }));

  passport.use('local-login', new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
  },
  function(req, email, password, done)
  {
    session = req.session || session;
    User.findOne({ 'local.email':  email }, function(err, user)
    {
      if (err)
          return done(err);
      if (!user)
          return done(null, false, req.flash('loginMessage', 'No user found.'));
      if (!user.validPassword(password))
          return done(null, false, req.flash('loginMessage', 'Wrong password.'));

      // --- przechwycenie emaila do sesji -------
      req.session.activeUser = email;
      return done(null, user);
    });
  }));
};
