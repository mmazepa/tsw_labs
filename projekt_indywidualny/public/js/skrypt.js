/*jshint jquery: true, devel: true, esversion: 6, browser: true */

document.onreadystatechange = function()
{
    if(document.readyState === "interactive")
    {
        $(function()
        {
            function disableNavigation()
            {
                let currentPage = parseInt(document.getElementById("currentPage").innerHTML);
                let pages = parseInt(document.getElementById("pagesCount").innerHTML);

                if (currentPage && pages)
                {
                    if (isNaN(window.location.href.split("/").pop()))
                        window.location.href = "/auctions/page/1";

                    if (currentPage == 1)
                        document.getElementById("pageLeft").disabled = true;
                    else
                        document.getElementById("pageLeft").disabled = false;

                    if (currentPage == pages || pages == 0)
                        document.getElementById("pageRight").disabled = true;
                    else
                        document.getElementById("pageRight").disabled = false;

                    if (currentPage > pages || currentPage < 0)
                    {
                        window.location.href = "/auctions/page/1";
                    }
                }
            }
            if (window.location.href.includes("auctions/page"))
            {
                disableNavigation();
            }

            function pagination()
            {
                scrollTop();
                disableNavigation();
            }

            $("#pageLeft").on("click", function()
            {
                window.location.href = "/auctions/page/" + (parseInt(document.getElementById("currentPage").innerHTML)-1);
                pagination();
            });

            $("#pageRight").on("click", function()
            {
                window.location.href = "/auctions/page/" + (parseInt(document.getElementById("currentPage").innerHTML)+1);
                pagination();
            });

            function selectMenuOption()
            {
                let currentLocation = window.location.href.split("/").pop();
                switch (currentLocation)
                {
                    case "":
                        document.getElementById("homepage").classList.add("activeMenuOption");
                        break;
                    case "auctions":
                        document.getElementById("auctions").classList.add("activeMenuOption");
                        break;
                    case "myAuctions":
                        document.getElementById("myAuctions").classList.add("activeMenuOption");
                        break;
                    case "signup":
                        document.getElementById("signup").classList.add("activeMenuOption");
                        break;
                    case "profile":
                        document.getElementById("profile").classList.add("activeMenuOption");
                        break;
                    case "messages":
                        document.getElementById("messageBox").classList.add("activeMenuOption");
                        break;
                    default:
                        break;
                }
            }
            selectMenuOption();

            $(".menuElement").on("click", function()
            {
                switch (this.id)
                {
                    case "homepage":
                        location.replace("/");
                        break;
                    case "auctions":
                        location.replace("/auctions/page/1");
                        break;
                    case "myAuctions":
                        location.replace("/myAuctions");
                        break;
                    case "signup":
                        location.replace("/signup");
                        break;
                    case "profile":
                        location.replace("/profile");
                        break;
                    case "messageBox":
                        location.replace("/auctions/messages");
                        break;
                    default:
                        break;
                }
            });

            $(".buyButton").on("click", function()
            {
                // ...
            });

            function prepareCurrentDate()
            {
                return prepareDate(new Date());
            }

            function prepareDate(date)
            {
                let timestamp = Date.parse(date);
                if (!isNaN(timestamp))
                {
                    let day = date.getDate();
                    let month = date.getMonth()+1;
                    let year = date.getFullYear();
                    let hours = date.getHours();
                    let minutes = date.getMinutes();

                    if (day < 10) day = '0' + day;
                    if (month < 10) month = '0' + month;
                    if (hours < 10) hours = '0' + hours;
                    if (minutes < 10) minutes = '0' + minutes;

                    date = day + '-' + month + '-' + year + " " + hours + ":" + minutes;
                }
                else
                {
                    date = "-";
                }
                return date;
            }

            function preparePrices()
            {
                let prices = Array.from(document.getElementsByClassName("price"));
                prices.map((item, index) =>
                {
                    let price = parseFloat(item.innerHTML);
                    price = price.toFixed(2);
                    item.innerHTML = price + " PLN";
                });
            }
            preparePrices();

            function prepareDates()
            {
                let createDates = Array.from(document.getElementsByClassName("createDate"));
                let expireDates = Array.from(document.getElementsByClassName("expireDate"));

                createDates.map((item, index) =>
                {
                    item.innerHTML = prepareDate(new Date(item.innerHTML));
                });
                expireDates.map((item, index) =>
                {
                    item.innerHTML = prepareDate(new Date(item.innerHTML));
                });
            }
            prepareDates();

            function prepareDescriptions()
            {
                let descriptions = Array.from(document.getElementsByClassName("description"));
                descriptions.map((item, index) =>
                {
                    if (item.innerHTML == null || item.innerHTML == "") item.innerHTML = "-";
                });
            }
            prepareDescriptions();

            function prepareAvailability()
            {
                let availabilities = Array.from(document.getElementsByClassName("isAvailable"));
                availabilities.map((item, index) =>
                {
                    item.style.fontWeight = "bold";
                    switch(item.innerHTML)
                    {
                        case "true":
                            item.innerHTML = "TAK";
                            item.style.color = "darkgreen";
                            break;
                        case "false":
                            item.innerHTML = "NIE";
                            item.style.color = "darkred";
                            break;
                        default:
                            break;
                    }
                });
            }
            prepareAvailability();

            function refreshClock()
            {
                let clock = document.getElementById("clock");
                let now = new Date();

                let hours = now.getHours();
                let minutes = now.getMinutes();
                let seconds = now.getSeconds();

                if (hours < 10) hours = '0' + hours;
                if (minutes < 10) minutes = '0' + minutes;
                if (seconds < 10) seconds = '0' + seconds;

                clock.innerHTML = hours + ":" + minutes + ":" + seconds;
                setTimeout(refreshClock, 500);
            }
            refreshClock();
        });

        window.onscroll = () =>
        {
            checkScroll();
        };

        let checkScroll = () =>
        {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20)
            {
                document.getElementById("scrollTop").style.display = "block";
            }
            else
            {
                document.getElementById("scrollTop").style.display = "none";
            }
        };

        let scrollTop = () =>
        {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        };

        $("#scrollTop").on("click", function()
        {
            scrollTop();
        });
    }
};
