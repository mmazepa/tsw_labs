//jshint node: true, esversion: 6

var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var nodeMailer = require('nodemailer');

var Auction = mongoose.model("Auction");
var router = express.Router();

var session;
router.get('/', function(req, res, next)
{
  session = req.session || session;
  res.render('index',
  {
      session: req.session,
      auctions: req.session.auctions || req.app.get("auctions"),
      message: req.flash("loginMessage")
  });
});

router.get("/myAuctions", function(req, res)
{
    session = req.session || session;
    Auction.find({}).exec(function(err, auctions)
    {
        if (err)
        {
            console.lof("Error:", err);
        }
        else
        {
            res.render("../views/auctions/myAuctions",
            {
                auctions: auctions,
                session: session
            });
        }
    });
});

router.get('/signup', function(req, res)
{
  session = req.session || session;
  res.render('signup.ejs',
  {
      message: req.flash('signupMessage'),
      session: session
  });
});

router.get('/profile', isLoggedIn, function(req, res)
{
  session = req.session || session;
  Auction.find({}).exec(function(err, auctions)
  {
    if (err)
    {
      console.log("Error:", err);
    }
    else
    {
      var newMessagesAmount = 0;
      for (var i = 0; i < auctions.length; i++)
      {
          for (var j = auctions[i].messages.length-1; j >= 0; j--)
          {
              if (auctions[i].messages[j].user != session.activeUser.split("@")[0])
              {
                  if (auctions[i].messages[j].user != auctions[i].author.split("@")[0])
                  {
                      newMessagesAmount++;
                  }
              }
              else
              {
                  break;
              }
          }
      }
      req.session.newMessagesAmount = newMessagesAmount;
      session = req.session || session;
      res.render('profile.ejs',
      {
          user: req.user || req.session.user,
          session: session
      });
    }
  });
});

router.get('/logout', function(req, res)
{
  session = req.session || session;
  req.session.destroy(function(err)
  {
      if(err)
      {
          console.log(err);
      }
      else
      {
          session.destroy();
          req.logout();
          res.redirect('/');
      }
  });
});

router.post('/signup', passport.authenticate('local-signup',
{
  successRedirect: '/profile',
  failureRedirect: '/signup',
  failureFlash: true,
}));

router.post('/login', passport.authenticate('local-login',
{
  successRedirect: '/profile',
  failureRedirect: '/',
  failureFlash: true,
}), function(req,res)
{
    session = req.session || session;
});

function preparePrice(price)
{
    price = parseFloat(price);
    price = price.toFixed(2);
    return price;
}

function prepareDate(date)
{
    let timestamp = Date.parse(date);
    if (!isNaN(timestamp))
    {
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();

        if (day < 10) day = '0' + day;
        if (month < 10) month = '0' + month;
        if (hours < 10) hours = '0' + hours;
        if (minutes < 10) minutes = '0' + minutes;

        date = day + '-' + month + '-' + year + " " + hours + ":" + minutes;
    }
    else
    {
        date = "-";
    }
    return date;
}

router.post('/sendmail/:id', function(req, res)
{
    session = req.session || session;
    let time = parseInt(req.body.futureMail);
    let auctionId = req.params.id;
    let email = session.activeUser;

    let frequency = req.body.mailType;
    console.log("[FREQ]: " + frequency);
    if (frequency == "once")
    {
        console.log("[Mail]: Powiadomienie zostanie wysłane za " + time + " minut.");
    }
    else if (frequency == "multiple")
    {
        console.log("[Mail]: Powiadomienie będzie wysyłane co " + time + " minut.");
    }

    let myInterval = setInterval(function()
    {
        Auction.findOne({_id: auctionId}).exec(function(err, auction)
        {
          if (err)
          {
            console.log("Error:", err);
          }
          else
          {
            console.log("[MAIL]: " + auction._id + ", " + email);

            let transporter = nodeMailer.createTransport(
            {
                host: "smtp.gmail.com",
                port: 465,
                secure: true,
                auth:
                {
                    user: "tsw.micro.ebay@gmail.com",
                    pass: "microebay1234"
                }
            });

            let mailOptions =
            {
                from: '"Micro Ebay" <tsw.micro.ebay@gmail.com>',
                to: email,
                subject: auction.title + " (przypomnienie)",
                html: "<h1>" + auction.title + "</h1>" +
                        "<p>" +
                            "Przypomnienie o stanie aukcji.<br/>" +
                            "Uwaga! Wiadomość wygenerowana automatycznie - nie odpowiadaj!" +
                        "</p>" +
                        "<p>" +
                            "<b>Autor:</b> " + auction.author + "<br/>" +
                            "<b>Typ:</b> " + auction.type + "<br/>" +
                            "<b>Aktualna cena:</b> " + preparePrice(auction.price) + " PLN<br/>" +
                            "<b>Data stworzenia:</b> " + prepareDate(auction.createDate) + "<br/>" +
                            "<b>Data wygaśnięcia:</b> " + (auction.expireDate ? prepareDate(auction.expireDate) : "-") + "<br/>" +
                            "<b>Opis:</b> " + (auction.description ? auction.description : "-") + "<br/>" +
                            "<b>Dostępny:</b> " + (auction.isAvailable ? "tak" : "nie") + "<br/>" +
                        "</p>" +
                        "<p>" +
                            "Dziękujemy za skorzystanie z naszych usług.<br/>" +
                            "<br/>" +
                            "Serdeczne pozdrowienia śle<br/>" +
                            "Serwer Micro Ebay" +
                        "</p>"
            };

            transporter.sendMail(mailOptions, (error, info) =>
            {
                if (error) console.log(error);
                console.log("Wiadomość mailowa %s wysłana: %s", info.messageId, info.response);
            });
          }
        });
        if (frequency == "once")
        {
            console.log("Interval cleared because of: frequency " + frequency);
            clearInterval(myInterval);
        }
    }, 1000*time*60);    // wysyłanie za n minut
    // }, 1000*time);          // wysyłanie za n sekund (do testowania)

    res.redirect("/auctions/show/" + auctionId);
});

module.exports = session;
module.exports = router;

function isLoggedIn(req, res, next)
{
  if (req.isAuthenticated())
      return next();
  res.redirect('/');
}
