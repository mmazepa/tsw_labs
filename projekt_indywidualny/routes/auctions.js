//jshint node: true, esversion: 6

var express = require('express');
var router = express.Router();

var auction = require("../controllers/AuctionController.js");

// WSZYSTKIE
router.get('/page/:page', auction.list);
router.get('/messages', auction.messages);

// CRUD
router.get('/show/:id', auction.show);
router.get('/create', auction.create);
router.post('/save', auction.save);
router.get('/edit/:id', auction.edit);
router.post('/update/:id', auction.update);
router.post('/delete/:id', auction.delete);

// KUP TERAZ / LICYTACJA
router.post('/buy/:id', auction.buy);
router.post('/bid/:id', auction.bid);

module.exports = router;
