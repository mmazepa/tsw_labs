var mongoose = require("mongoose");
var Auction = mongoose.model("Auction");
var auctionController = {};

var session;

auctionController.list = function(req, res)
{
    session = req.session || session;
    page = parseInt(req.params.page) || 1;
    perPage = 5;
    var paginatedAuctions = [];
    Auction.find({}).exec(function(err, auctions)
    {
        if (err)
        {
            console.lof("Error:", err);
        }
        else
        {
            for (var i = page*perPage - perPage; i < page*perPage; i++)
            {
                if (i >= auctions.length || i < 0) break;
                paginatedAuctions.push(auctions[i]);
            }

            var pageInfo =
            {
                page: page,
                pages: Math.ceil(auctions.length/perPage)
            };
            res.render("../views/auctions/index",
            {
                auctions: paginatedAuctions,
                pageInfo: pageInfo,
                session: session
            });
        }
    });
};

auctionController.messages = function(req, res)
{
    session = req.session || session;
    session.newMessagesAmount = req.session.newMessages || 0;
    Auction.find({}).exec(function(err, auctions)
    {
      if (err)
      {
        console.log("Error:", err);
      }
      else
      {
        res.render("../views/auctions/messages",
        {
            auctions: auctions,
            session: session
        });
      }
    });
};

auctionController.show = function(req, res)
{
  session = req.session || session;
  Auction.findOne({_id: req.params.id}).exec(function(err, auction)
  {
    if (err)
    {
      console.log("Error:", err);
    }
    else
    {
      res.render("../views/auctions/show",
      {
          auction: auction,
          session: session
      });
    }
  });
};

auctionController.create = function(req, res)
{
  session = req.session || session;
  res.render("../views/auctions/create",
  {
      session: session
  });
};

auctionController.save = function(req, res)
{
  session = req.session || session;
  req.body.createDate = new Date();
  req.body.author = session.activeUser;
  if (req.body.type === "Kup teraz!")
  {
      req.body.duration = null;
      req.body.expireDate = null;
  }
  else if (req.body.type === "Licytacja")
  {
      if (!duration) req.body.duration = 14;
      req.body.expireDate = new Date(req.body.createDate.getTime() + 1000 * 3600 * 24 * req.body.duration);
  }
  req.body.isAvailable = true;

  var auction = new Auction(req.body);

  auction.save(function(err)
  {
    session = req.session || session;
    if (err)
    {
      console.log(err);
      res.render("../views/auctions/create",
      {
          session: session
      });
    }
    else
    {
      console.log("Aukcja stworzona pomyślnie.");
      res.redirect("/auctions/show/" + auction._id);
    }
  });
};

auctionController.edit = function(req, res)
{
  session = req.session || session;
  Auction.findOne({_id: req.params.id}).exec(function(err, auction)
  {
    if (err)
    {
      console.log("Error:", err);
    }
    else
    {
      res.render("../views/auctions/edit",
      {
          auction: auction,
          session: session
      });
    }
  });
};

auctionController.update = function(req, res)
{
  session = req.session || session;
  // if (req.body.type === "Kup teraz!")
  // {
  //     req.body.duration = null;
  //     req.body.expireDate = null;
  // }
  // else if (req.body.type === "Licytacja")
  // {
  //     if (!duration) req.body.duration = 14;
  //     req.body.expireDate = new Date(new Date(req.body.createDate).getTime() + 1000 * 3600 * 24 * req.body.duration);
  // }

  if (new Date(req.body.expireDate).getTime() <= new Date().getTime())
  {
      req.body.isAvailable = false;
  }
  else
  {
      req.body.isAvailable = true;
  }
  Auction.findByIdAndUpdate(req.params.id,
  {
      $set:
      {
        title: req.body.title,
        // author: req.body.author,
        // type: req.body.type,
        price: req.body.price,
        createDate: req.body.createDate,
        // duration: req.body.duration,
        expireDate: req.body.expireDate,
        description: req.body.description,
        isAvailable: req.body.isAvailable
      }
  },
  { new: true },
  function(err, auction)
  {
    if (err)
    {
      console.log(err);
      res.render("../views/auctions/edit",
      {
          auction: req.body
      });
    }
    res.redirect("/auctions/show/" + auction._id);
  });
};

auctionController.delete = function(req, res)
{
  session = req.session || session;
  Auction.remove({ _id: req.params.id }, function(err)
  {
    if(err)
    {
      console.log(err);
    }
    else
    {
      console.log("Aukcja usunięta!");
      res.redirect("/auctions/page/1");
    }
  });
};

auctionController.buy = function(req, res)
{
    session = req.session || session;
    buyer =
    {
        email: session.activeUser,
        date: new Date(),
    };
    Auction.findByIdAndUpdate(req.params.id,
    {
        $set:
        {
          isAvailable: false,
          expireDate: buyer.date,
          buyer: buyer
        }
    },
    { new: true },
    function(err, auction)
    {
      if (err)
      {
        console.log(err);
      }
      res.redirect("../show/" + auction._id);
    });
};

auctionController.bid = function(req, res)
{
    session = req.session || session;
    offer =
    {
        email: session.activeUser,
        date: new Date(),
        bid: req.body.bid
    };
    Auction.findByIdAndUpdate(req.params.id,
    {
        $push:
        {
            bids: offer
        },
        $set:
        {
            price: offer.bid
        }
    },
    { new: true },
    function(err, auction)
    {
        if (err)
        {
            console.log(err);
        }
        res.redirect("../show/" + auction._id);
    });
};

module.exports = auctionController;
