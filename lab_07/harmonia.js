//jshint browser: true, esversion:6

// ZALECENIA:
//  * użyć własności/reagować na stan "interactive"
//  * użyć własności Document.readyState
//  * querySelector, querySelectorAll

document.onreadystatechange = function()
{
    if(document.readyState === "interactive")
    {
        var divs = Array.from(document.querySelectorAll("div.hd"));
        divs.map((item, index) =>
        {
            item.nextElementSibling.classList.add("visible");
            var wasOpen = true;

            item.onclick = function()
            {
                if(item.nextElementSibling.classList.contains("visible") && wasOpen)
                {
                    item.nextElementSibling.classList.remove("visible");
                    item.nextElementSibling.classList.add("unvisible");
                    wasOpen = false;
                }
                else
                {
                    item.nextElementSibling.classList.remove("unvisible");
                    item.nextElementSibling.classList.add("visible");
                    wasOpen = true;
                }
            };

            item.onmouseover = function()
            {
                if(item.nextElementSibling.classList.contains("unvisible"))
                {
                    item.nextElementSibling.classList.remove("unvisible");
                    item.nextElementSibling.classList.add("visible");
                }
            };

            item.onmouseout = function()
            {
                if(item.nextElementSibling.classList.contains("visible") && !wasOpen)
                {
                    item.nextElementSibling.classList.remove("visible");
                    item.nextElementSibling.classList.add("unvisible");
                }
            };
        });
    }
};
